package com.ramirez.roadmap.client.utils;

import java.time.LocalDate;
import java.time.Period;

public class Utils {
    public static int getAgeByBirthDate(LocalDate date){
        return Period.between(date,LocalDate.now()).getYears();
    }

    public static LocalDate getAgeToBirthDate(int age){
        return LocalDate.now().minusYears(age);
    }
}
