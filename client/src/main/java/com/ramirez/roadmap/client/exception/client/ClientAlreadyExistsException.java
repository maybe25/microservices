package com.ramirez.roadmap.client.exception.client;


public class ClientAlreadyExistsException extends RuntimeException{

    public ClientAlreadyExistsException(String DocType, String ClientNumDoc) {
        super(String.format("Client with Num Document: %s number: %s Already Exists", DocType,ClientNumDoc));
    }
}

