package com.ramirez.roadmap.client.mappers;
import com.ramirez.roadmap.client.dto.ClientRequestDTO;
import com.ramirez.roadmap.client.entity.Client;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ClientRequestMapper {
    ClientRequestMapper INSTANCE = Mappers.getMapper(ClientRequestMapper.class);


    public Client clientRequestDTOToClient(ClientRequestDTO clientRequestDTO);

    public ClientRequestDTO clientToRequestDTO(Client client);
}

