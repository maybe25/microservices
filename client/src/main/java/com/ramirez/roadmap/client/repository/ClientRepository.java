package com.ramirez.roadmap.client.repository;

import com.ramirez.roadmap.client.entity.Client;
import com.ramirez.roadmap.client.entity.DocumentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client,Long> {

    public Client findByNumDoc(String numDoc);

    public Client findByDocumentTypeAndNumDoc(DocumentType documentType, String numDoc);

    public List<Client> findAllByBirthDateLessThan(LocalDate birthDate);


}