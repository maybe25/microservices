package com.ramirez.roadmap.client.exception.client;


public class ClientYearsException extends RuntimeException{

    public ClientYearsException() {
        super(String.format("El cliente a insertar debe ser mayor de 18 años "));
    }
}
