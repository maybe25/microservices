package com.ramirez.roadmap.client.controller;
import com.ramirez.roadmap.client.dto.ClientDTO;
import com.ramirez.roadmap.client.dto.ClientRequestDTO;
import com.ramirez.roadmap.client.service.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT,RequestMethod.PATCH})
@RequestMapping(value = "/clients", consumes = MediaType.ALL_VALUE)
@Validated
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Operation(summary = "Insertar cliente")
    @PostMapping(produces = { "application/json"})
    @ResponseBody
    @ResponseStatus(value = HttpStatus.CREATED)
    public ClientDTO insertarFotoAndClient(@Valid @RequestPart("client") ClientRequestDTO client) throws IOException {return clientService.insertClient(client);}

    @Operation(summary = "Actualizar cliente")
    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ClientDTO updateClientRequestDTO(@RequestBody ClientRequestDTO clientRequestDTO) {return clientService.updateClient(clientRequestDTO);}

    @Operation(summary = "Eliminar un objeto Persona e ImagenPersona por ID Persona")
    @DeleteMapping("/{id}")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<String> deleteClient(@PathVariable(name = "id")Long id) {
        clientService.deleteClient(id);
        return ResponseEntity.ok("Persona correctamente eliminada con id: "+id);
    }

    @Operation(summary = "Obtener Client tipo y numero numero de documento")
    @GetMapping("/document")
    @ResponseStatus(HttpStatus.OK)
    public ClientDTO findClientByDocumentTypeAndNumDoc(@RequestParam("numDoc") String numDoc,
                                                       @RequestParam(value = "documentType",required = false)  Long idDocumentType){
        if(idDocumentType==null){
            return clientService.findClientByNumDoc(numDoc);
        }
        return clientService.findClientByDocTypeAndDocNum(idDocumentType,numDoc);
    }

    @Operation(summary = "Obtener informacion de Cliente mayores a ")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<ClientDTO> getClientsDTO(@RequestParam(value = "gt")int age){return clientService.listAllClientAgeLessThan(age);}

    @Operation(summary = "Obtener Cliente por ID")
    @GetMapping("/{id}")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<ClientDTO> getClientById(@PathVariable(name = "id")Long id){
        return ResponseEntity.ok(clientService.findClientById(id));
    }




}
