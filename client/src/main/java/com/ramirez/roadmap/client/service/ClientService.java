package com.ramirez.roadmap.client.service;

import com.ramirez.roadmap.client.dto.ClientDTO;
import com.ramirez.roadmap.client.dto.ClientRequestDTO;

import java.util.List;

public interface ClientService {

    public List<ClientDTO> listAllClientAgeLessThan(int age);
    public ClientDTO findClientByNumDoc(String numDoc);
    public ClientDTO findClientById(Long id);
    public ClientDTO findClientByDocTypeAndDocNum(Long documentType,String numDoc);
    public ClientDTO insertClient(ClientRequestDTO client);
    public ClientDTO updateClient(ClientRequestDTO client);
    public void deleteClient(Long id);
}
