package com.ramirez.roadmap.client.dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClientDTO{
    private Long idClient;
    @NotBlank
    private String name;
    @NotBlank
    private String lastName;
    @NotBlank
    private String cityBirth;
    @NotNull
    private int age;
    @NotBlank
    private DocumentTypeDTO documentType;
    @NotBlank
    private String numDoc;

    private String imageUrl;
}
