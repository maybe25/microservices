package com.ramirez.roadmap.client.exception.client;

public class ClientNotFoundException extends RuntimeException  {

    public ClientNotFoundException(String numDoc) {
        super(String.format("Client with Document Number: %s does not exists", numDoc));
    }

    public ClientNotFoundException(Long id) {
        super(String.format("Client with id number: %s does not exists", id));
    }

    public ClientNotFoundException(Long id,String numDoc) {
        super(String.format("Client with document number: %s does not exists or Document Type with id: %s not associated  ", numDoc,id));
    }


}
