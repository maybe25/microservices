package com.ramirez.roadmap.client.dto;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class ClientRequestDTO {


    private Long idClient;
    @NotBlank
    private String name;
    @NotNull
    @NotBlank
    private String lastName;
    @NotBlank
    private String cityBirth;
    @NotBlank
    private LocalDate birthDate;
    @NotBlank
    private DocumentTypeDTO documentType;
    @NotBlank
    private String numDoc;
}