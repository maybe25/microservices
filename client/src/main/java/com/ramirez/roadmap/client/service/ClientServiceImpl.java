package com.ramirez.roadmap.client.service;

import com.ramirez.roadmap.client.dto.ClientDTO;
import com.ramirez.roadmap.client.dto.ClientRequestDTO;
import com.ramirez.roadmap.client.dto.ImageDTO;
import com.ramirez.roadmap.client.entity.Client;
import com.ramirez.roadmap.client.entity.DocumentType;
import com.ramirez.roadmap.client.exception.client.ClientAlreadyExistsException;
import com.ramirez.roadmap.client.exception.client.ClientNotFoundException;
import com.ramirez.roadmap.client.exception.client.ClientYearsException;
import com.ramirez.roadmap.client.mappers.ClientMapper;
import com.ramirez.roadmap.client.mappers.ClientRequestMapper;
import com.ramirez.roadmap.client.repository.ClientRepository;
import com.ramirez.roadmap.client.service.apiservice.ImageApiService;
import com.ramirez.roadmap.client.utils.Utils;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService{

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ImageApiService imageApiService;



    @Override
    public ClientDTO insertClient(ClientRequestDTO client) {
        Client clientVerify = clientRepository.findByNumDoc(client.getNumDoc());
        if(clientVerify!=null) throw new ClientAlreadyExistsException(clientVerify.getDocumentType().getDescription(),clientVerify.getNumDoc());
        if(Utils.getAgeByBirthDate(client.getBirthDate())<=18) throw new ClientYearsException();

        Client clientObj = ClientRequestMapper.INSTANCE.clientRequestDTOToClient(client);
        clientRepository.save(clientObj);
        ClientDTO responseObj = ClientMapper.INSTANCE.mapToDto(clientObj);
        return responseObj;
    }


    @Override
    public List<ClientDTO> listAllClientAgeLessThan(int age) {
        List<ClientDTO> lstDTO = ClientMapper.INSTANCE.mapToDTO(clientRepository.findAllByBirthDateLessThan(Utils.getAgeToBirthDate(age)));
        for(ClientDTO clientDTO:lstDTO){
            try {
                ResponseEntity<ImageDTO> image = imageApiService.getImageByNumDoc(clientDTO.getNumDoc());
                    clientDTO.setImageUrl(image.getBody().getImageUrl());

            }catch (FeignException e){
                /*System.out.println(e.status());
                System.out.println(e.getMessage());
                e.printStackTrace();*/
                clientDTO.setImageUrl("No photo assigned yet");
            }


        }
        return lstDTO;
    }

    @Override
    public ClientDTO findClientByNumDoc(String numDoc) {
        Client clientVerify = clientRepository.findByNumDoc(numDoc);
        if(clientVerify==null)throw new ClientNotFoundException(numDoc);
        return (ClientMapper.INSTANCE.mapToDto(clientVerify));
    }

    @Override
    public ClientDTO findClientById(Long id) {
        ClientDTO clientVerify = ClientMapper.INSTANCE.mapToDto(clientRepository.findById(id).orElseThrow(()-> new ClientNotFoundException(id)));
        /*ImageDTO image =restTemplate.getForObject("http://localhost:8080/"+clientVerify.getIdClient()+"/images",ImageDTO.class);
        if(image==null){
            clientVerify.setImageUrl("No photo assigned yet");
        }else{
            clientVerify.setImageUrl(image.getImageUrl());
        }*/
        return clientVerify;
    }

    @Override
    public ClientDTO findClientByDocTypeAndDocNum(Long documentType, String numDoc) {
        DocumentType docType = new DocumentType();
        docType.setIdDocType(documentType);
        ClientDTO objDTO = ClientMapper.INSTANCE.mapToDto(clientRepository.findByDocumentTypeAndNumDoc(docType,numDoc));
        if(objDTO == null)throw new ClientNotFoundException(documentType,numDoc);
        return objDTO;
    }


    @Override
    public ClientDTO updateClient(ClientRequestDTO client) {
        clientRepository.findById(client.getIdClient()).orElseThrow(()->new ClientNotFoundException(client.getIdClient()));
        Client clientResponse = clientRepository.save(ClientRequestMapper.INSTANCE.clientRequestDTOToClient(client));
        return ClientMapper.INSTANCE.mapToDto(clientResponse);
    }

    @Override
    public void deleteClient(Long id) {
            clientRepository.findById(id).orElseThrow(()->new ClientNotFoundException(id));
            clientRepository.deleteById(id);
            //restTemplate.delete("http://localhost:8080/"+id);
    }
}
