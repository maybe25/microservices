package com.ramirez.roadmap.client.service.apiservice;


import com.ramirez.roadmap.client.dto.ImageDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient("IMAGE-SERVICE")
public interface ImageApiService {


    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ImageDTO> getImageByIdClient(@RequestParam(name = "idClient")Long id);



    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<ImageDTO> getImageByNumDoc(@RequestParam(name = "numDoc")String numDoc);


}
