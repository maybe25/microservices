package com.ramirez.roadmap.client.mappers;

import com.ramirez.roadmap.client.dto.ClientDTO;
import com.ramirez.roadmap.client.entity.Client;
import com.ramirez.roadmap.client.utils.Utils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface ClientMapper  {
    ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);


    @Mapping(source = "birthDate",target = "age",qualifiedByName = "birthDateToAge")
    public ClientDTO mapToDto(Client client);

    @Named("birthDateToAge")
    public static int birthDateToAge(LocalDate date){
        return Utils.getAgeByBirthDate(date);
    }


    @Mapping(source = "birthDate",target = "age",qualifiedByName = "birthDateToAge")
    public List<ClientDTO> mapToDTO(List<Client> listClient);

    @Mapping(source = "age",target = "birthDate", qualifiedByName = "ageToBirthDate")
    public Client DTOtoEntity(ClientDTO clientDTO);

    @Named("ageToBirthDate")
    public static LocalDate ageToBirthDate(int age){
        return Utils.getAgeToBirthDate(age);
    }
}