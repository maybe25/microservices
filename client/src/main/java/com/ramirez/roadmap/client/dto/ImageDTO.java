package com.ramirez.roadmap.client.dto;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ImageDTO {

    private String id;

    private String name;

    private String imageUrl;

    private String imageId;

    private String client;
}