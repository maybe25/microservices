package com.ramirez.roadmap.image.service;

import com.ramirez.roadmap.image.dto.ImageDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {

    public ImageDTO getImageByIdClient(Long idClient);
    public ImageDTO getImageByIdImage(String idImage);
    public ImageDTO getImageByNumDoc(String numDoc);
    public ImageDTO insertImage(Long idClient, MultipartFile image)throws IOException;
    public void deleteImage(Long id) throws IOException;
}
