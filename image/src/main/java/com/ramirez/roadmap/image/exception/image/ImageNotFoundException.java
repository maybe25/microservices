package com.ramirez.roadmap.image.exception.image;

public class ImageNotFoundException extends RuntimeException{
    public ImageNotFoundException(String id) {
        super(String.format("Image associated with client number document: %s not found",id));
    }

    public ImageNotFoundException(Long id) {
        super(String.format("Image associated with client number document: %s not found",id));
    }
}