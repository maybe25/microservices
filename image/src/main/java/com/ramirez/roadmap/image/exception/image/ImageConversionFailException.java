package com.ramirez.roadmap.image.exception.image;


public class ImageConversionFailException extends RuntimeException{
    public ImageConversionFailException() {
        super("The image couldn't convert to base64");
    }
}