package com.ramirez.roadmap.image.service;

import com.ramirez.roadmap.image.dto.ClientDTO;
import com.ramirez.roadmap.image.dto.ImageDTO;
import com.ramirez.roadmap.image.entity.Image;
import com.ramirez.roadmap.image.exception.client.ClientNotFoundException;
import com.ramirez.roadmap.image.exception.image.ImageNotFoundException;
import com.ramirez.roadmap.image.mapper.ImageMapper;
import com.ramirez.roadmap.image.repository.ImageRepository;
import com.ramirez.roadmap.image.service.apiservice.ClientApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@Service
public class ImageServiceImpl implements ImageService{

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private CloudinaryService cloudinaryService;

    @Autowired
    private ClientApiService clientApiService;



    @Override
    public ImageDTO getImageByIdClient(Long idClient) {
        ClientDTO clientDTO = clientApiService.getClientById(idClient);
        Image image = imageRepository.findByClientNumDoc(clientDTO.getNumDoc());
        if(image==null){throw new ImageNotFoundException(clientDTO.getNumDoc());}
        return ImageMapper.INSTANCE.mapToDto(image);
    }

    @Override
    public ImageDTO getImageByIdImage(String idImage) {
        return ImageMapper.INSTANCE.mapToDto(imageRepository.findById(idImage).orElseThrow());
    }

    @Override
    public ImageDTO getImageByNumDoc(String numDoc) {
        Image image = imageRepository.findByClientNumDoc(numDoc);
        if(image==null){throw new ImageNotFoundException(numDoc);}
        return ImageMapper.INSTANCE.mapToDto(image);
    }

    @Override
    public ImageDTO insertImage( Long idClient, MultipartFile image) throws IOException {
        ClientDTO clientDTO = clientApiService.getClientById(idClient);
        Optional.of(clientDTO).orElseThrow(()->new ClientNotFoundException(idClient));
        Image imageVerify = imageRepository.findByClientNumDoc(clientDTO.getNumDoc());
        if(imageVerify!=null){
            imageRepository.deleteById(imageVerify.getId());
            cloudinaryService.delete(imageVerify.getImageId());
        }
        BufferedImage bi = ImageIO.read(image.getInputStream());
        Image imageObj = new Image();

        Map result = cloudinaryService.upload(image);
        imageObj = new Image((String)result.get("original_filename"), (String)result.get("url"), (String)result.get("public_id"));

        imageObj.setClient(clientDTO.getNumDoc());
        return ImageMapper.INSTANCE.mapToDto(imageRepository.save(imageObj));
    }

    @Override
    public void deleteImage(Long id) throws IOException {
        ClientDTO clientDTO = clientApiService.getClientById(id);
        Optional.of(clientDTO).orElseThrow(()->new ClientNotFoundException(id));
        Image image = imageRepository.findByClientNumDoc(clientDTO.getNumDoc());
        if(image==null){throw new ImageNotFoundException(clientDTO.getNumDoc());}
        imageRepository.deleteById(image.getId());
        cloudinaryService.delete(image.getImageId());
    }
}
