package com.ramirez.roadmap.image.controller;
import com.ramirez.roadmap.image.dto.ImageDTO;
import com.ramirez.roadmap.image.service.ImageService;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RequestMapping(value = "/clients/images", consumes = MediaType.ALL_VALUE)
public class ImageController {

    private static final String CLIENT_SERVICE = "clientcb";

    @Autowired
    private ImageService imageService;

    @CircuitBreaker(name = CLIENT_SERVICE, fallbackMethod = "fallSaveImageByUser")
    @Operation(summary = "Insertar y Actualizar imagen a cliente por Id cliente ")
    @PostMapping(value = "/{id}",produces = { "application/json"})
    @ResponseBody
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity<ImageDTO> insertarFotoAndPersona(@RequestPart(value = "image",required = false) MultipartFile image,
                                                           @PathVariable("id") Long id) throws IOException {return ResponseEntity.ok(imageService.insertImage(id,image));}



    /*@CircuitBreaker(name = CLIENT_SERVICE, fallbackMethod = "fallBackGetImageByUserId")*/
    @Operation(summary = "Obtener image por ID cliente (Param)")
    @GetMapping(value = "/{id}")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<ImageDTO> getImageByIdClient(@PathVariable(name = "id")String id){return ResponseEntity.ok(imageService.getImageByIdImage(id));}


    @Operation(summary = "Obtener image por ID cliente (Param)")
    @GetMapping
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<ImageDTO> getImageByNumDoc(@RequestParam(name = "numDoc")String numDoc){
        return ResponseEntity.ok(imageService.getImageByNumDoc(numDoc));
    }


    /*@CircuitBreaker(name = "clientCB", fallbackMethod = "fallBackGetImageByUser")*/
    @Operation(summary = "Eliminar foto de usuario")
    @DeleteMapping(value = "/{id}")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public String deleteImageByNumDoc(@PathVariable(name = "id")Long id) throws IOException{
        imageService.deleteImage(id);
        return "Imagen eliminada correctamente con id Image: "+id;
    }


    @CircuitBreaker(name = CLIENT_SERVICE, fallbackMethod = "fallBackGetImageByUser")
    @Operation(summary = "Obtener image por ID cliente (Param)")
    @GetMapping("/document")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<ImageDTO> getImageByIdImage(@RequestParam(name = "idClient" , required = false)Long idClient,
                                                      @RequestParam(name = "numDoc" ,required = false)String numDoc){
        if(idClient==null){
            return ResponseEntity.ok(imageService.getImageByNumDoc(numDoc));
        }
        return ResponseEntity.ok(imageService.getImageByIdClient(idClient));
    }


    /*Metodos para la recepcion de los fallbacks de Client*/

    public ResponseEntity<ImageDTO> fallBackGetImageByUser(@RequestParam(name = "idClient" , required = false)Long idClient,
                                                           @RequestParam(name = "numDoc" ,required = false)String numDoc,
                                                           RuntimeException e){
        return new ResponseEntity("El cliente "+idClient+" no esta dispuesto ahora mismo para insercion",HttpStatus.OK);
    }

    public ResponseEntity<ImageDTO> fallSaveImageByUser(@RequestPart(value = "image",required = false) MultipartFile image,
                                                        @PathVariable("id") Long id,
                                                        RuntimeException e) {
        return new ResponseEntity("El cliente no esta dispuesto ahora mismo",HttpStatus.OK);
    }

    /*public ResponseEntity<ImageDTO> fallBackGetImageByUserId(@PathVariable(name = "id")String id,
                                                        RuntimeException e) {
        return new ResponseEntity("El cliente no esta dispuesto ahora mismo "+id,HttpStatus.OK);
    }*/
}