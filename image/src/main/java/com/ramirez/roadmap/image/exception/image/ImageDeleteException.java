package com.ramirez.roadmap.image.exception.image;


public class ImageDeleteException extends RuntimeException{
    public ImageDeleteException(String id) {
        super(String.format("An error occurred while trying to delete the image with id %s The Image couldn't delete",id));
    }
}