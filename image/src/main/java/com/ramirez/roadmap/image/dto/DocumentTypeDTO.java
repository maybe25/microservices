package com.ramirez.roadmap.image.dto;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DocumentTypeDTO {

    @NotBlank
    private Long idDocType;
    @NotBlank
    private String description;
}
