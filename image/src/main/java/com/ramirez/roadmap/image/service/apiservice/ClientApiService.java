package com.ramirez.roadmap.image.service.apiservice;

import com.ramirez.roadmap.image.dto.ClientDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(name = "client-service",url = "http://localhost:8090/clients")
public interface ClientApiService {

    @RequestMapping(method = RequestMethod.GET,value = "/{id}")
    public ClientDTO getClientById(@PathVariable("id")Long id);
}
