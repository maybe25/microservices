package com.ramirez.roadmap.image.repository;

import com.ramirez.roadmap.image.entity.Image;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends MongoRepository<Image,String> {
    @Query(value = "{'client':?0}")
    Image findByClientNumDoc(String numDoc);

    @Query(value = "{'client':?0}",fields = "{'imageUrl':0}")
    Image findImageNameByClient(String numDoc);
}
