package com.ramirez.roadmap.image.dto;
import lombok.Data;
import javax.validation.constraints.NotBlank;


@Data
public class ImageDTO {

    private String id;
    @NotBlank
    private String name;
    @NotBlank
    private String imageUrl;
    @NotBlank
    private String imageId;
    @NotBlank
    private String client;
}

