package com.ramirez.roadmap.image.mapper;

import com.ramirez.roadmap.image.dto.ImageDTO;
import com.ramirez.roadmap.image.entity.Image;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ImageMapper {

    ImageMapper INSTANCE = Mappers.getMapper(ImageMapper.class);

    public ImageDTO mapToDto(Image image);
    public List<ImageDTO> mapToDTO(List<Image> listImage);
}
